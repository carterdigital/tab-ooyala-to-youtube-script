import pandas as pd

raw_db_file = open("db/edit.txt", "r")
raw_db = raw_db_file.read()


df = pd.read_csv("csv/data.csv", header=0)
data = {}

i = 0
n = 0
y = 0

for row in df.itertuples():

    o_id = str(row._3)
    if o_id == "":
        continue
    else:
        yt_id = str(row._4)

        if "https://www.youtube.com/watch?v=" in yt_id:
            yt_id = yt_id.replace("https://www.youtube.com/watch?v=", "")
            data[i] = {o_id : yt_id}
            raw_db = raw_db.replace(o_id, yt_id)
            print ("(√) Found and replaced: " + o_id + " -> " + yt_id)
            n = n +1
        else:
            y = y +1
            print ("(X) No YouTube Link found for : " + o_id)


    i = i + 1


new_db = open("db/new.txt", "w")
new_db.writelines(raw_db)
new_db.close()
print("\n\n------------------------")
print("Complete")
print("------------------------\n\n")
print("Results:\n")
print("Replaced " + str(n) + " Ooyala IDs.")
print(str(y) + " YouTube links not found.")
print("------------------------\n")